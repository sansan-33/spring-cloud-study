package com.san3.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.san3.entity.User;
import com.san3.repository.UserRepository;

@Service
public class UserService {

	public User findById(@PathVariable Long id) {
		User findOne = new User();
		findOne.setAge(19);
		findOne.setId(id);
		findOne.setName("马云");
		findOne.setUsername("马化腾");
	    return findOne;
	}
}
