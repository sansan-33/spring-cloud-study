package com.san3.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.san3.entity.User;
import com.san3.service.UserService;

@RestController
public class UserController {
  @Autowired
  private UserService userService;

  @GetMapping("/{id}")
  public User findById(@PathVariable Long id) {
    User findOne = this.userService.findById(id);
    return findOne;
  }
}
