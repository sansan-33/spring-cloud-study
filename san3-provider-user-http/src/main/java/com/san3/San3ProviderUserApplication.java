package com.san3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class San3ProviderUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(San3ProviderUserApplication.class, args);
	}

}
